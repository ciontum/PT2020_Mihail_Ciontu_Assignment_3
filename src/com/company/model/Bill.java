package com.company.model;


/**
 * Model that we use mainly to objectify database results
 */
public class Bill {
    private int id;
    private String clientName;
    private String productName;
    private String city;
    private int stoc;

    public Bill(int id, String clientName, String productName, String city,int stoc) {
        this.id = id;
        this.clientName = clientName;
        this.productName = productName;
        this.city = city;
        this.stoc=stoc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    public int getStoc(){
        return stoc;
    }
}
