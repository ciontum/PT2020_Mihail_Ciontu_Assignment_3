package com.company.model;

/**
 * Model that we use mainly to objectify database results
 */
public class Product {
    private String name;
    private int stoc;
    private double price;

    public Product(String name, int stoc, double price) {
        this.name = name;
        this.stoc = stoc;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getStoc() {
        return stoc;
    }

    public void setStoc(int stoc) {
        this.stoc = stoc;
    }


}
