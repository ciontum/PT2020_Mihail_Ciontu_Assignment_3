package com.company.model;

/**
 * Model that we use mainly to objectify database results
 */
public class Order {
    private String clientName;
    private String productName;
    private int stoc;

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getStoc() {
        return stoc;
    }

    public void setStoc(int stoc) {
        this.stoc = stoc;
    }

    public Order(String clientName, String productName, int stoc) {
        this.clientName = clientName;
        this.productName = productName;
        this.stoc = stoc;
    }


}
