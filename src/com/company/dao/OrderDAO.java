package com.company.dao;

import com.company.connection.ConnectionFactory;
import com.company.model.Bill;
import com.company.model.Order;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * OrderDAO is the data access class , this is the only possible gate to query the database,the results goes back to
 * {@link com.company.bll.OrderBLL}

 */
public class OrderDAO {

    protected static final Logger LOGGER = Logger.getLogger(OrderDAO.class.getName());
    private static final String createTableString = "CREATE TABLE IF NOT EXISTS orders (clientName TEXT,productName TEXT,stoc INTEGER)";
    public static final String dropTableString = "DROP TABLE IF EXISTS orders";
    private static final String selectProductString = "SELECT stoc FROM products WHERE name=?";
    public static final String insertOrderString = "INSERT INTO orders VALUES(?,?,?)";
    private static final String updateProductString = "UPDATE products SET stoc=? WHERE products.name=?";
    private static final String selectClientOrderString = "SELECT firstname || ' ' || lastname AS name ,orders.productName,orders.stoc,city FROM clients INNER JOIN orders ON orders.clientName=name WHERE name=?";
    private  static final String selectOrdersString="SELECT * FROM orders";
    public static void createTable() {
        Connection dbConnection = ConnectionFactory.getConnection();
        try {
            Statement statement = dbConnection.createStatement();
            statement.execute(createTableString);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(dbConnection);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ClientDAO:createClient " + e.getMessage());
        }
    }

    public static void dropTable() {
        Connection dbConnection = ConnectionFactory.getConnection();
        Statement statement = null;
        try {
            statement = dbConnection.createStatement();
            statement.execute(dropTableString);
            ;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(dbConnection);
        }
    }

    public static int insertOrder(Order order) {

        try (Connection dbConnection = ConnectionFactory.getConnection();
             PreparedStatement insertOrderStatement = dbConnection.prepareStatement(insertOrderString);
             PreparedStatement selectProductStatement = dbConnection.prepareStatement(selectProductString);
             PreparedStatement updateProductStatement = dbConnection.prepareStatement(updateProductString)) {
            selectProductStatement.setString(1, order.getProductName());
            updateProductStatement.setString(1, String.valueOf(order.getStoc()));
            updateProductStatement.setString(2, order.getProductName());
            ResultSet rs = selectProductStatement.executeQuery();
            if (rs.next()) {
                int currentStoc = rs.getInt(1);
                if (order.getStoc() > currentStoc)
                    return -1;
                updateProductStatement.execute();
                insertOrderStatement.setString(1, order.getClientName());
                insertOrderStatement.setString(2, order.getProductName());
                insertOrderStatement.setString(3, String.valueOf(order.getStoc()));
                insertOrderStatement.execute();
            } else {
                return -2;
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
            e.printStackTrace();
        }
        return 1;
    }

    public static Bill createBill(String name) {
        Bill bill=new Bill(-1,"","","",0);
        try (Connection dbConnection = ConnectionFactory.getConnection();
             PreparedStatement selectClientOrder = dbConnection.prepareStatement(selectClientOrderString)
        ) {
            selectClientOrder.setString(1, name);
            ResultSet rs = selectClientOrder.executeQuery();
            rs.next();
            bill=new Bill((int) (Math.random() * 100),
                                        name,
                                        rs.getString("productName"),
                                        rs.getString("city"),
                                        rs.getInt("stoc"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return bill;

    }
    public static List<Order> queryOrders(){


        List<Order> orders=new ArrayList<>();
        try(Connection dbConnection=ConnectionFactory.getConnection();
            PreparedStatement selectStatement=dbConnection.prepareStatement(selectOrdersString);
            ResultSet rs=selectStatement.executeQuery()){
            while(rs.next()){
                Order order=new Order(rs.getString("clientName"),rs.getString("productName")
                        ,Integer.parseInt(rs.getString("stoc")));
                orders.add(order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return orders;
    }
}
