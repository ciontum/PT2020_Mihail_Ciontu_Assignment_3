package com.company.dao;

import com.company.connection.ConnectionFactory;
import com.company.model.Client;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ClientDAO is the data access class , this is the only possible gate to query the database,the results goes back to
 * {@link com.company.bll.ClientBLL}

 */
public class ClientDAO {
    protected static final Logger LOGGER=Logger.getLogger(ClientDAO.class.getName());
    private static final String createTableString="CREATE TABLE IF NOT EXISTS clients (firstname TEXT,lastname TEXT,city TEXT)";
    public static final String dropTableString="DROP TABLE IF EXISTS clients";
    private static final String insertClient="INSERT INTO clients VALUES(?,?,?)";
    public static final String selectClients="SELECT * FROM clients";
    public static final String deleteClient="DELETE FROM clients WHERE firstname=? AND lastname=?";
    public static void createTable(){
        Connection dbConnection= ConnectionFactory.getConnection();
        try{
            Statement statement=dbConnection.createStatement();
            statement.execute(createTableString);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(dbConnection);
        }catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ClientDAO:createClient " + e.getMessage());
        }
    }

    public static void dropTable() {
        Connection dbConnection=ConnectionFactory.getConnection();
        Statement statement=null;
        try{
            statement=dbConnection.createStatement();
            statement.execute(dropTableString);
            ;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(dbConnection);
        }
    }
    public static int insertClient(Client client){
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertClient,Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, client.getFirstname());
              insertStatement.setString(2, client.getLastname());
               insertStatement.setString(3, client.getCity());
                insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "StudentDAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }

        return insertedId;
    }
    public static void deleteClient(String firstname,String lastname){
        Connection dbConnection=ConnectionFactory.getConnection();
        try (PreparedStatement deleteStatement = dbConnection.prepareStatement(deleteClient)) {
            deleteStatement.setString(1,firstname);
            deleteStatement.setString(2,lastname);
            deleteStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static List<Client> queryClient(){
        Connection dbConnection=ConnectionFactory.getConnection();

        PreparedStatement selectStatement =null;
        ResultSet rs=null;

        List<Client> clients=new ArrayList<>();
        try{
            selectStatement=dbConnection.prepareStatement(selectClients);
            rs=selectStatement.executeQuery();
            while(rs.next()){
                Client client=new Client(rs.getString("firstname"),rs.getString("lastname"),rs.getString("city"));
                clients.add(client);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(selectStatement);
            ConnectionFactory.close(rs);
            ConnectionFactory.close(dbConnection);

        }

        return clients;
    }
}
