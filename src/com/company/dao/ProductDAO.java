package com.company.dao;

import com.company.connection.ConnectionFactory;
import com.company.model.Product;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * ProductDAO is the data access class , this is the only possible gate to query the database,the results goes back to
 * {@link com.company.bll.ProductBLL}

 */
public class ProductDAO {
    protected static final Logger LOGGER=Logger.getLogger(ProductDAO.class.getName());
    private static final String createTableString="CREATE TABLE IF NOT EXISTS products (name TEXT,stoc INTEGER,price INTEGER)";
    private static final String dropTableString="DROP TABLE IF EXISTS products";
    private static final String insertProducts="INSERT INTO products VALUES(?,?,?)";
    private static final String selectProductString="SELECT stoc FROM products WHERE name=?";
    private static final String updateProductString="UPDATE products SET stoc=? WHERE products.name=?";
    private static final String deleteProductString="DELETE FROM products WHERE name= ? ";
    private static final String selectProductsString="SELECT * FROM products";
    public static void createTable(){
        Connection dbConnection= ConnectionFactory.getConnection();
        try{
            Statement statement=dbConnection.createStatement();
            statement.execute(createTableString);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(dbConnection);
        }catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ClientDAO:createClient " + e.getMessage());
        }
    }

    public static void dropTable() {
        Connection dbConnection=ConnectionFactory.getConnection();
        Statement statement=null;
        try{
            statement=dbConnection.createStatement();
            statement.execute(dropTableString);
            ;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(dbConnection);
        }
    }
    public static void updateProductStoc(Product product,Connection dbConnection){
        try(PreparedStatement updateStatement=dbConnection.prepareStatement(updateProductString)){
            updateStatement.setString(1, String.valueOf(product.getStoc()));
            updateStatement.setString(2, product.getName());
            updateStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void insertProduct(Product product){

        PreparedStatement insertStatement = null;
        ResultSet rs=null;
        try ( Connection dbConnection = ConnectionFactory.getConnection();
              PreparedStatement selectStatement=dbConnection.prepareStatement(selectProductString)){
            selectStatement.setString(1,product.getName());
            rs=selectStatement.executeQuery();
            if (rs.next()) {
                product.setStoc(rs.getInt(1)+product.getStoc());
                updateProductStoc(product,dbConnection);
            }
            else
            {
                insertStatement=dbConnection.prepareStatement(insertProducts);
                insertStatement.setString(1,product.getName());
                insertStatement.setString(2,String.valueOf(product.getStoc()));
                insertStatement.setString(3,String.valueOf(product.getPrice()));
                insertStatement.execute();
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(insertStatement);
        }

    }
    public static void deleteProduct(String produtctName){
        try(Connection dbConnection=ConnectionFactory.getConnection();
            PreparedStatement deleteStatement=dbConnection.prepareStatement(deleteProductString)) {
            deleteStatement.setString(1,produtctName);
            deleteStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static List<Product> queryProducts(){


        List<Product> products=new ArrayList<>();
        try(Connection dbConnection=ConnectionFactory.getConnection();
            PreparedStatement selectStatement=dbConnection.prepareStatement(selectProductsString);
            ResultSet rs=selectStatement.executeQuery()){
            while(rs.next()){
                Product product=new Product(rs.getString("name"),Integer.parseInt(rs.getString("stoc"))
                                                        ,Double.parseDouble(rs.getString("price")));
                products.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return products;
    }
}
