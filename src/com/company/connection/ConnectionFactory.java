package com.company.connection;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ConnectionFactory has the logic for
 *  ->creating and getting database connections
 *  ->closeing statements,db connections and result sets
 */
public class ConnectionFactory {
    private static final String DBURL = "jdbc:sqlite:D:\\db\\test.db";
    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static ConnectionFactory singleInstance = new ConnectionFactory();

    private Connection createConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(DBURL);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "An error occured while trying to connect to the DB");
            e.printStackTrace();
        }
        return connection;
    }

    public static Connection getConnection() {
        return singleInstance.createConnection();
    }

    public static void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, "An error occured while trying to close the connection");
            }
        }
    }

    public static void close(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, "An error occured while trying to close the statement");
            }
        }
    }
    public static void close(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, "An error occured while trying to close the ResultSet");
            }
        }
    }
}
