package com.company.bll;

import com.company.dao.ProductDAO;
import com.company.model.Product;

import java.util.List;

/**
 * ProductBLL is the bussiness logic class that works like a middleman between {@link com.company.presentation.Controller}
 * and {@link com.company.dao.ProductDAO}
 *
 */
public class ProductBLL {
    public void createTable(){
        ProductDAO.createTable();
    }
    public void dropTabel(){
        ProductDAO.dropTable();
    }
    public void insertProduct(Product product){
        ProductDAO.insertProduct(product);
    }
    public void deleteProduct(String productName){
        ProductDAO.deleteProduct(productName);
    }
    public List<Product> getAllProducts(){
        return ProductDAO.queryProducts();
    }
}
