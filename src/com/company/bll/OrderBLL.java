package com.company.bll;

import com.company.dao.OrderDAO;
import com.company.model.Bill;
import com.company.model.Order;
/**
 * OrderBLL is the bussiness logic class that works like a middleman between {@link com.company.presentation.Controller}
 * and {@link com.company.dao.OrderDAO}
 *
 */
import java.util.List;

public class OrderBLL {
    public void createTable(){
        OrderDAO.createTable();
    }
    public void dropTabel(){
        OrderDAO.dropTable();
    }
    public int insertOrder(Order order){
        return OrderDAO.insertOrder(order);
    }
    public Bill createBill(String clientName){
        return OrderDAO.createBill(clientName);
    }
    public List<Order> getAllOrders(){
        return OrderDAO.queryOrders();
    }
}
