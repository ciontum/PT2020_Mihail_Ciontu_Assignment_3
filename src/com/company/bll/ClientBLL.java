package com.company.bll;

import com.company.dao.ClientDAO;
import com.company.model.Client;

import java.util.List;
/**
 * ClientBLL is the bussiness logic class that works like a middleman between {@link com.company.presentation.Controller}
 * and {@link com.company.dao.ClientDAO}
 *
 */
public class ClientBLL {
public int insertClient(Client client){
return ClientDAO.insertClient(client);
}
public void deleteClient(String firstname,String lastname){
     ClientDAO.deleteClient(firstname,lastname);
}
public List<Client> getAllClients(){
    return ClientDAO.queryClient();
}
public void dropTable(){
    ClientDAO.dropTable();
}
public void createTable(){
    ClientDAO.createTable();
}
}
