package com.company.presentation;

import com.company.bll.ClientBLL;
import com.company.bll.OrderBLL;
import com.company.bll.ProductBLL;
import com.company.model.Bill;
import com.company.model.Client;
import com.company.model.Order;
import com.company.model.Product;
import com.itextpdf.text.DocumentException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Controller is used to parse and send commands from the input file to the BLLs' .
 * It also has the logic to send parsed information to the {@link com.company.presentation.View}
 */
public class Controller {
    BufferedReader reader;
    ClientBLL client;
    ProductBLL product;
    OrderBLL order;
    View view;
    int clientRNumber=1,productRNumber=1,orderRNumber=1;
    List<Client> clientsReports;
    List<Product> productReports;
    List<Order> ordersReports;
    private String findFile(String fileName){
        Class myClass=getClass();
        ClassLoader loader=myClass.getClassLoader();
        URL myURL=loader.getResource(fileName);
        String path= myURL.getPath();
        path=path.replaceAll("%20"," ");
        System.out.println(path);
        return path;
    }
    private void initializeReader(String parameter){
        try{
            reader=new BufferedReader(new FileReader("C:/Users/ciont/IdeaProjects/PT2020_Mihail_Ciontu_Assignment_3/commands.txt"));
        }catch(FileNotFoundException e){
            System.out.println("File not found"+parameter);
        }
    }
    public void parseCommands(){
        try {
            String line = reader.readLine();
            while(line!=null){
                String[] splited=line.split(":");
                if(splited.length==1) {
                    computeReports(splited[0].split(" ")[1].trim());
                }
                    else
                    switch (splited[0]) {
                        case "Order": {
                            String[] splitedValues=splited[1].split(",");
                            computeOrder(splitedValues[0].trim(),splitedValues[1].trim(),Integer.parseInt(splitedValues[2].trim()));
                            break;
                        }
                        case "Insert client":{
                            String[] splitedValues = splited[1].split(",");
                            computeClient(splitedValues[0].trim(), splitedValues[1].trim(),"insert");
                            break;
                        }
                        case "Delete client":{
                            computeClient(splited[1].trim(),null,"delete");
                            break;
                        }
                        case "Insert product":{
                            String[] splitedValues=splited[1].split(",");
                            computeProduct(splitedValues[0].trim(),
                                    Integer.parseInt(splitedValues[1].trim()),
                                    Double.parseDouble(splitedValues[2].trim()),"insert");
                            break;
                        }
                        case "Delete product":{
                            computeProduct(splited[1].trim(),0,0,"delete");
                            break;
                        }
                    }
                    line=reader.readLine();
            }
        } catch (IOException | DocumentException e) {
            e.printStackTrace();
        }
    }
    private void computeClient(String name,String city,String operation){
        String firstname=name.split(" ")[0].trim();
        String lastname=name.split(" ")[1].trim();
        if(operation.equals("insert")){
            Client clientModel=new Client(firstname,lastname,city);
            client.insertClient(clientModel);
        }
        else
            client.deleteClient(firstname,lastname);

    }
    private void computeProduct(String name,int stoc,double price,String operation){
        if(operation.equals("insert")){
            Product productModel=new Product(name,stoc,price);
            product.insertProduct(productModel);
        }
        else
            product.deleteProduct(name);

    }
    private boolean computeOrder(String clientName,String productName,int stoc) throws IOException, DocumentException {
        Order orderInfo=new Order(clientName,productName,stoc);
        int executedOrder=order.insertOrder(orderInfo);
        if(executedOrder==-1) {
            view.createBillReport(new Bill((int) (Math.random() * 100), clientName, productName, null, 0), false);
            return false;
        }
        else
            if(executedOrder==-2)
                return false;


        Bill bill=order.createBill(clientName);
        view.createBillReport(bill,true);
        return true;
    }
    private void computeReports(String type){
        if(type.equals("client")) {
            clientsReports=client.getAllClients();
            view.createClientReport(clientsReports,clientRNumber);
            clientRNumber++;
        }
        if(type.equals("product")) {
            productReports=product.getAllProducts();
            view.createProductReport(productReports,productRNumber);
            productRNumber++;
        }
        if(type.equals("order")){
            ordersReports=order.getAllOrders();
            view.createOrderReport(ordersReports,orderRNumber);
            orderRNumber++;
        }
    }
    public Controller(String parameter){
        initializeReader(parameter);
        view=new View();
        client=new ClientBLL();
        product=new ProductBLL();
        order=new OrderBLL();
        client.dropTable();
        client.createTable();
        product.dropTabel();
        product.createTable();
        order.dropTabel();
        order.createTable();
        parseCommands();

    }

}
