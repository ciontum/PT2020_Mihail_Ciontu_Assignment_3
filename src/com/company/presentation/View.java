package com.company.presentation;

import com.company.model.Bill;
import com.company.model.Order;
import com.company.model.Product;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.company.model.Client;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * View class is used to output database statements that we are interested in.
 * Instead of a GUI , it uses a PDF generator library so we list the queryes mainly in table format
 */
public class View {

    public View(){
    }
    private static void addTableHeader(PdfPTable table, List<String> headerTitles){
        for(String columnTitle : headerTitles) {
            PdfPCell header = new PdfPCell();
            header.setBackgroundColor(BaseColor.LIGHT_GRAY);
            header.setBorderWidth(2);
            header.setPhrase(new Phrase(columnTitle));
            table.addCell(header);
        }
    }
    public void createClientReport(List<Client> clients,int number){
        Document document=new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream("reports/clients/clients-"+number+".pdf"));
            document.open();
            PdfPTable table =new PdfPTable(3);
            List<String> headerTitles=new ArrayList<>();
            headerTitles.add("Prenume");
            headerTitles.add("Nume");
            headerTitles.add("Oras");
            addTableHeader(table,headerTitles);
            for(Client client : clients){
                table.addCell(client.getFirstname());
                table.addCell(client.getLastname());
                table.addCell(client.getCity());
            }
            document.add(table);
            document.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
    public void createProductReport(List<Product> products,int number){
        Document document=new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream("reports/products/products-"+number+".pdf"));
            document.open();
            PdfPTable table =new PdfPTable(3);
            List<String> headerTitles=new ArrayList<>();
            headerTitles.add("Nume");
            headerTitles.add("Stoc");
            headerTitles.add("Pret");
            addTableHeader(table,headerTitles);
            for(Product product : products){
                table.addCell(product.getName());
                table.addCell(String.valueOf(product.getStoc()));
                table.addCell(String.valueOf(product.getPrice()));
            }
            document.add(table);
            document.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
    public void createOrderReport(List<Order> orders,int number){
        Document document=new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream("reports/orders/orders-"+number+".pdf"));
            document.open();
            PdfPTable table =new PdfPTable(4);
            List<String> headerTitles=new ArrayList<>();
            headerTitles.add("Nume");
            headerTitles.add("Produs");
            headerTitles.add("Stoc");
            addTableHeader(table,headerTitles);
            for(Order order : orders){
                table.addCell(order.getClientName());
                table.addCell(order.getProductName());
                table.addCell(String.valueOf(order.getStoc()));
            }
            document.add(table);
            document.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
    public void createBillReport(Bill bill,boolean isStoc) throws IOException, DocumentException {
        Document document=new Document();
        PdfWriter.getInstance(document,new FileOutputStream("orders/Order-"+bill.getClientName()+"-"+bill.getId()+".pdf"));
        if(isStoc) {
            document.open();
            Font font= FontFactory.getFont(FontFactory.COURIER_BOLD,16,BaseColor.BLACK);
            Chunk chunk=new Chunk(bill.getClientName() + " " + bill.getProductName() + " " + bill.getStoc() + "buc" + " " + bill.getCity(),font);
            document.add(chunk);
            document.close();
        }
        else
        {
            document.open();
            Font font= FontFactory.getFont(FontFactory.COURIER_BOLD,16,BaseColor.BLACK);
            Chunk chunk=new Chunk(bill.getProductName()+" is out of stock!",font);
            document.add(chunk);
            document.close();

        }

    }
}
